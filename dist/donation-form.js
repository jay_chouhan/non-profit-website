const form = document.getElementById('form');

const errorMsg = document.getElementById('errorMsg');
const alphaOnlyReg = new RegExp('[a-zA-Z][a-zA-Z ]+');
const strAddrReg = new RegExp('^[#.0-9a-zA-Zs,-]+$');
const zipReg = new RegExp('[0-9]');
const emailReg = new RegExp('^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$');
const ccNumReg = new RegExp('^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14})$');
const cvvReg = new RegExp('[0-9]');

const firstName = document.forms['donationForm']['firstname'];
const lastName = document.forms['donationForm']['lastname'];
const strAddress = document.forms['donationForm']['strAddress'];
const city = document.forms['donationForm']['city'];
const state = document.forms['donationForm']['state'];
const zip = document.forms['donationForm']['zipcode'];
const email = document.forms['donationForm']['email'];
const ccNum = document.forms['donationForm']['ccnum'];
const expMonth = document.forms['donationForm']['expMonth'];
const expYear = document.forms['donationForm']['expYear'];
const cvv = document.forms['donationForm']['securitycode'];

form.addEventListener('submit', function (event) {
  if (!alphaOnlyReg.test(firstName.value)) {
    errorMsg.classList.remove('hide');
    errorMsg.innerText = '* Please provide a valid first name ';
    firstName.focus();
    event.preventDefault();
  } else if (!alphaOnlyReg.test(lastName.value)) {
    errorMsg.classList.remove('hide');
    errorMsg.innerText = '* Please provide a valid last name ';
    lastName.focus();
    event.preventDefault();
  } else if (!strAddrReg.test(strAddress.value)) {
    errorMsg.classList.remove('hide');
    errorMsg.innerText = '* Please provide a valid street address ';
    strAddress.focus();
    event.preventDefault();
  } else if (!alphaOnlyReg.test(city.value)) {
    errorMsg.classList.remove('hide');
    errorMsg.innerText = '* Please provide a valid city name ';
    city.focus();
    event.preventDefault();
  } else if (state.selectedIndex < 1) {
    errorMsg.classList.remove('hide');
    errorMsg.innerText = '* Please choose your state ';
    state.focus();
    event.preventDefault();
  } else if (!zipReg.test(zip.value)) {
    errorMsg.classList.remove('hide');
    errorMsg.innerText = '* Please provide a valid zip code';
    zip.focus();
    event.preventDefault();
  } else if (!emailReg.test(email.value)) {
    errorMsg.classList.remove('hide');
    errorMsg.innerText = '* Please provide a valid email id';
    email.focus();
    event.preventDefault();
  } else if (!ccNumReg.test(ccNum.value)) {
    errorMsg.classList.remove('hide');
    errorMsg.innerText = '* Please provide a valid credit card number';
    ccNum.focus();
    event.preventDefault();
  } else if (expMonth.selectedIndex < 1) {
    errorMsg.classList.remove('hide');
    errorMsg.innerText = '* Please give card expiry month';
    expMonth.focus();
    event.preventDefault();
  } else if (expYear.selectedIndex < 1) {
    errorMsg.classList.remove('hide');
    errorMsg.innerText = '* Please give card expiry year';
    expYear.focus();
    event.preventDefault();
  } else if (!cvvReg.test(cvv.value) && cvv.value.length !== 3) {
    errorMsg.classList.remove('hide');
    errorMsg.innerText = '* Please provide a valid cvv number';
    cvv.focus();
    event.preventDefault();
  }
});
